json.array!(@events) do |event|
  json.extract! event, :id, :event_start_date, :concert_id
  json.url event_url(event, format: :json)
end
