class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :event_start_date
      t.references :concert, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
