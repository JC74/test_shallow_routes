class CreateConcerts < ActiveRecord::Migration
  def change
    create_table :concerts do |t|
      t.string :body
      t.string :artist
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
